package hu.zolkiss.kotlin

import hu.zolkiss.kotlin.classes.HtmlElement
import hu.zolkiss.kotlin.classes.render
import java.text.SimpleDateFormat
import java.util.*


fun main(args: Array<String>) {
    val jsonDateFormat = SimpleDateFormat(JSON_DATE_TEMPLATE).format(Date())
    println("Hello, World at $jsonDateFormat")

    val htmlElement = HtmlElement("plainText")
    htmlElement.content = "Lorem ipsum"
    htmlElement.attributes.put("name", "plain-text")

    val html = htmlElement.render()
    println(html)
}