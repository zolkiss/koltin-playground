package hu.zolkiss.kotlin.classes

import hu.zolkiss.kotlin.functions.renderHtml
import java.util.*

data class HtmlElement(
    var tagName: String,
    var content: String = "",
    var attributes: LinkedHashMap<String, String> = LinkedHashMap(),
    var styles: LinkedHashMap<String, String> = LinkedHashMap(),
    var children: LinkedList<HtmlElement> = LinkedList()
)

fun HtmlElement.render(): String {
    return renderHtml(this)
}

fun HtmlElement.attr(key: String, value: String): HtmlElement {
    this.attributes.put(key, value)
    return this
}

fun HtmlElement.attr(keyValue: Pair<String, String>): HtmlElement {
    return attr(keyValue.first, keyValue.second)
}

fun HtmlElement.style(name: String, value: String): HtmlElement {
    this.styles.put(name, value)
    return this
}

fun HtmlElement.style(style: Pair<String, String>): HtmlElement {
    return style(style.first, style.second)
}

fun HtmlElement.addChild(child: HtmlElement): HtmlElement {
    this.children.add(child)
    return this;
}

fun HtmlElement.addChildren(vararg children: HtmlElement): HtmlElement {
    this.children.addAll(children)
    return this;
}