package hu.zolkiss.kotlin.functions

import hu.zolkiss.kotlin.classes.HtmlElement
import hu.zolkiss.kotlin.classes.render
import org.apache.commons.lang3.StringUtils.isNotEmpty

fun renderHtml(element: HtmlElement): String {

    val elementBuilder = StringBuilder("<${element.tagName}")

    addAttributes(elementBuilder, element)
    addStyles(elementBuilder, element)
    addContent(elementBuilder, element)
    return elementBuilder.toString()
}

private fun addAttributes(elementBuilder: StringBuilder, element: HtmlElement) {
    element.attributes.forEach { key, value -> elementBuilder.append(" $key=\"$value\"") }
}

private fun addContent(elementBuilder: StringBuilder, element: HtmlElement) {
    elementBuilder.append(if (hasInnerContent(element)) getInnerContent(element) else "/>")
}

private fun getInnerContent(element: HtmlElement) = ">${element.children.map { c -> c.render() }.joinToString("")}${element.content}</${element.tagName}>"

private fun hasInnerContent(element: HtmlElement) = isNotEmpty(element.content) || element.children.isNotEmpty()

private fun addStyles(elementBuilder: StringBuilder, element: HtmlElement) {
    if (element.styles.isEmpty()) {
        return
    }

    elementBuilder.append(" styles=\"")
        .append(element.styles.map { entry -> "${entry.key}:${entry.value}" }.joinToString(";")) //
        .append("\"")
}