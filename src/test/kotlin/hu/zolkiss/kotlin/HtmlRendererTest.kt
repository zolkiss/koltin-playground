package hu.zolkiss.kotlin

import hu.zolkiss.kotlin.classes.*
import org.hamcrest.Matchers.equalTo
import org.hamcrest.junit.MatcherAssert.assertThat
import org.junit.Test

class HtmlRendererTest {

    @Test
    fun simpleElement() {
        assertThat(HtmlElement("simple").render(), equalTo("<simple/>"))
    }

    @Test
    fun simpleElementWithContent() {
        val tagName = "simple"
        val content = "simpleContent"
        assertThat(HtmlElement(tagName, content).render(), equalTo("<$tagName>$content</$tagName>"))
    }

    @Test
    fun simpleElementWithOneAttribute() {
        val tagName = "simple"
        val attribute = Pair("name", "simpleAttr")
        val htmlElement = HtmlElement(tagName)
        htmlElement.attributes.put(attribute.first, attribute.second)

        assertThat(htmlElement.render(), equalTo("<$tagName ${attribute.first}=\"${attribute.second}\"/>"))
    }

    @Test
    fun simpleElementWithContentAndMultipleAttributes() {
        val tagName = "simple"
        val content = "simpleContent"
        val attrOne = Pair("nameOne", "simpleAttrOne")
        val attrTwo = Pair("nameTwo", "simpleAttrTwo")
        val attrThree = Pair("nameThree", "simpleAttrThree")
        val htmlElement = HtmlElement(tagName, content)
            .attr(attrOne)
            .attr(attrTwo)
            .attr(attrThree)

        assertThat(htmlElement.render(), equalTo("<$tagName" +
            " ${attrOne.first}=\"${attrOne.second}\"" +
            " ${attrTwo.first}=\"${attrTwo.second}\"" +
            " ${attrThree.first}=\"${attrThree.second}\">$content</$tagName>"))
    }

    @Test
    fun simpleElementWithOneStyleAndOneAttribute() {
        val tagName = "simple"
        val attr = Pair("name", "value")
        val style = Pair("text-align", "center")

        val rendered = HtmlElement(tagName).attr(attr).style(style).render()
        assertThat(rendered, equalTo("<$tagName ${attr.first}=\"${attr.second}\" styles=\"${style.first}:${style.second}\"/>"))
    }

    @Test
    fun simpleElementWithTwoStylesAndOneAttribute() {
        val tagName = "simple"
        val attr = Pair("name", "value")
        val styleOne = Pair("text-align", "center")
        val styleTwo = Pair("vertical-align", "middle")

        val rendered = HtmlElement(tagName)
            .attr(attr)
            .style(styleOne)
            .style(styleTwo)
            .render()

        assertThat(rendered, equalTo("<$tagName" +
            " ${attr.first}=\"${attr.second}\"" +
            " styles=\"${styleOne.first}:${styleOne.second};${styleTwo.first}:${styleTwo.second}\"" +
            "/>")
        )
    }

    @Test
    fun complexElementWithoutStyleAndAttribute() {
        val wrapper = HtmlElement("div")
        val childOne = HtmlElement("input")

        assertThat(wrapper.addChild(childOne).render(), equalTo("<div><input/></div>"))
    }

    @Test
    fun multiLevelComplexElementWithStylesAndAttributes() {
        val mainWrapper = HtmlElement("div", "div_text_content").attr("id", "main-wrapper")
        val inputTag = HtmlElement("input").attr("type", "password").style("text-align", "middle")
        val aTag = HtmlElement("a", "SuperLink").attr("href", "#")

        val rendered = mainWrapper.addChildren(inputTag, aTag).render()
        assertThat(rendered, equalTo("<div id=\"main-wrapper\">" +
            "<input type=\"password\" styles=\"text-align:middle\"/>" +
            "<a href=\"#\">SuperLink</a>" +
            "div_text_content</div>"))
    }
}
